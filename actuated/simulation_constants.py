TIMESTEP = 1
TIMESTEP_STR = str(TIMESTEP)

MOVEMENTS = ['from_south_to_north','from_west_to_north', 
	     'from_west_to_east', 'from_north_to_east', 
             'from_north_to_south', 'from_east_to_south',
             'from_east_to_west', 'from_south_to_west']

LANE_ID_LOOKUP = {'from_south_to_north': 'from_south_0', 'from_west_to_north': 'from_west_1',
                  'from_west_to_east': 'from_west_0', 'from_north_to_east': 'from_north_1',
                  'from_north_to_south': 'from_north_0', 'from_east_to_south': 'from_east_1',
                  'from_east_to_west': 'from_east_0', 'from_south_to_west': 'from_south_1'}

VALID_COMBO_LIST = [{'from_north_to_south','from_north_to_east'}, {'from_north_to_south','from_south_to_north'}, 
					{'from_north_to_east','from_south_to_west'}, 
					{'from_east_to_west','from_east_to_south'}, {'from_east_to_west','from_west_to_east'},
					{'from_east_to_south','from_west_to_north'},
					{'from_south_to_north','from_south_to_west'},
					{'from_west_to_east','from_west_to_north'}]

MOVEMENT_TO_INDEX = {'from_north_to_south': 0, 'from_north_to_east': 1, 'from_east_to_west': 2, 'from_east_to_south': 3,
					'from_south_to_north': 4, 'from_south_to_west': 5, 'from_west_to_east': 6, 'from_west_to_north': 7}

# MOVEMENTS = ['updown','leftright']

NAME_TO_CODE = {'updown': 0, 'leftright': 2}

INITIAL_FEE = 1
SATURATION_HEADWAY = 3/TIMESTEP
INITIAL_BANK = 1000

IN_PROGRESS = -1
WIN = 1
LOSS = 0
NOT_ALLOWED = -2

G_MAX = 5/TIMESTEP
G_EXT = 3/TIMESTEP

CLEARANCE = 4/TIMESTEP

ALPHA = 1
BETA = 1

TRAFFIC_LIGHT_ID = "gneJ26"

ON = 'G'
OFF = 'r'
AMBER = 'y'

SERVICE_DIST = 86

V_TYPES = ['type1', 'type2', 'type3', 'type4']

PREAMBLE = """<routes>
        <vType id="type1" accel="0.8" decel="3.5" sigma="0.5" length="5" minGap="6" maxSpeed="25" departSpeed="max" guiShape="passenger"/>
        <vType id="type2" accel="0.7" decel="4.5" sigma="0.5" length="4" minGap="4" maxSpeed="30" departSpeed="max" guiShape="passenger"/>
        <vType id="type3" accel="0.7" decel="6.5" sigma="0.5" length="6" minGap="2" maxSpeed="20" departSpeed="max" guiShape="passenger"/>
        <vType id="type4" accel="0.9" decel="5.0" sigma="0.5" length="5" minGap="3.5" maxSpeed="25" departSpeed="max" guiShape="passenger"/>

        <route id="from_south_to_north" edges="from_south to_north" />
        <route id="from_west_to_north" edges="from_west to_north" />

        <route id="from_west_to_east" edges="from_west to_east" />
        <route id="from_north_to_east" edges="from_north to_east" />

        <route id="from_north_to_south" edges="from_north to_south" />
        <route id="from_east_to_south" edges="from_east to_south" />
        
        <route id="from_east_to_west" edges="from_east to_west" />
        <route id="from_south_to_west" edges="from_south to_west" />"""
        
MAF = 100
