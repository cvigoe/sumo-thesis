from __future__ import division

import pymc3 as pm
import numpy as np
from simulation_constants import *

import seaborn as sns
import matplotlib.pyplot as plt


class Driver(object):

    def __init__(self, driver_id, vot, initial_queue_pos, movement_manager, traci, saturation_headway=SATURATION_HEADWAY, bank=INITIAL_BANK, initial_fee=INITIAL_FEE, visible=False):
        self.driver_id = driver_id
        self.vot = vot                  # Value of Time - assume fixed for a given driver
        self.queue_pos = initial_queue_pos
        self.saturation_headway = saturation_headway
        self.bank = bank
        self.movement_manager = movement_manager
        self.alpha_prior = 1
        self.alpha_posterior = -1
        self.beta_prior = 1
        self.beta_posterior = -1
        self.k = 0
        self.n = 0
        self.timer = 0
        self.visible = visible

        self.set_up_initial_road_and_lane(traci)

        self.set_up_rho()
        self.set_up_estimated_delay()
    

    ####################################################################################################################################
    # Initialisation functions
    ####################################################################################################################################

    def set_up_initial_road_and_lane(self, traci):
        self.initial_road = traci.vehicle.getRoadID(self.driver_id)
        self.initial_lane = traci.vehicle.getLaneID(self.driver_id)

    def set_up_rho(self):
        self.rho = (self.alpha_prior + self.beta_prior) / self.alpha_prior      # Analytical form for mean(X) wehre X ~ Beta(alpha, beta)
    
    def set_up_estimated_delay(self):
        self.initial_estimated_delay = self.queue_pos*self.saturation_headway*self.rho
        self.estimated_delay = self.initial_estimated_delay

    def pay_initial_fee(self, initial_fee):
        self.bank = self.bank - initial_fee     # Assume a driver can always pay initial fee for now
        self.movement_manager.receive_initial_fee(initial_fee)

    
    ####################################################################################################################################
    # Update functions
    ####################################################################################################################################
    
    def update_result(self):
        result = self.movement_manager.bidding_history[-1][1]
        
        if result == WIN:
            print('\tOutcome was WIN')
            print('\tOld k: ' + str(self.k) + '\t New k: ' + str(self.k + 1))
            print('\tOld n: ' + str(self.n) + '\t New n: ' + str(self.n + 1))
            self.k = self.k + 1
            self.n = self.n + 1
        elif result == LOSS:
            print('\tOutcome was LOSS')
            print('\tOld k: ' + str(self.k) + '\t New k: ' + str(self.k))
            print('\tOld n: ' + str(self.n) + '\t New n: ' + str(self.n + 1))
            self.n = self.n + 1
        elif result == NOT_ALLOWED:
            print('\tWas not allowed participate.')

    def update_theta_belief(self):
        self.alpha_posterior = self.alpha_prior + self.k
        print('\tAlpha prior: ' + str(self.alpha_prior) + '\t Alpha posterior: ' + str(self.alpha_posterior))
        self.beta_posterior = self.beta_prior + self.n - self.k
        print('\tBeta prior: ' + str(self.beta_prior) + '\t Beta posterior: ' + str(self.beta_posterior))
        new_rho = (self.alpha_posterior + self.beta_posterior) / self.alpha_posterior
        print('\tOld rho: ' + str(self.rho) + '\t New rho: ' + str(new_rho))
        self.rho = new_rho

    def update_queue_pos(self):
        queue = self.movement_manager.queue 
        new_queue_pos = list(queue).index(self.driver_id) + 1
        print('\tOld queue pos: ' + str(self.queue_pos) + '\t Updated queue pos: ' + str(new_queue_pos))
        self.queue_pos = new_queue_pos

    def update_timer(self):
        new_time = self.timer + self.movement_manager.municipality.timeout + self.movement_manager.municipality.clearance*CLEARANCE
        print('\tOld timer: ' + str(self.timer) + '\t Updated timer: ' + str(new_time))
        self.timer = new_time

    def update_estimated_delay(self):
        new_delay = self.timer + (self.queue_pos*self.saturation_headway*self.rho)                          
        print('\tOld est. delay: ' + str(self.estimated_delay) + '\t Updated est. delay: ' + str(new_delay) + '\t Initial est. delay: ' + str(self.initial_estimated_delay))
        self.estimated_delay = new_delay

    def update_vot(self):
        print('\tOld VOT: ' + str(self.vot) + '\t Updated VOT: ' + str(self.vot))

    def take_posterior_as_prior(self):
        self.alpha_prior = self.alpha_posterior
        self.beta_prior = self.beta_posterior


    ####################################################################################################################################
    # Action functions
    ####################################################################################################################################    

    def decide_on_voluntary_contribution(self):
        bayes_factor = self.compute_bayes_factor()
        voluntary_contribution = self.vot*(10/(1 + 10*np.exp(self.initial_estimated_delay - self.estimated_delay)))/bayes_factor
        print('\tVoluntary contribution: ' + str(voluntary_contribution))
        return voluntary_contribution

    def compute_bayes_factor(self):
        return (self.alpha_posterior / (self.alpha_posterior + self.beta_posterior)) / (self.alpha_prior / (self.alpha_prior + self.beta_prior))

    def make_voluntary_contribution(self, voluntary_contribution):
        self.bank = self.bank - voluntary_contribution
        self.movement_manager.receive_voluntary_contribution(voluntary_contribution)        
