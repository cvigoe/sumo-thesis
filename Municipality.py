from __future__ import division

import pymc3 as pm
from simulation_constants import *
import textwrap as textwrap


class Municipality(object):

    def __init__(self, bank=INITIAL_FEE):
        self.bank = bank
        self.bids = {}                          # bids = { movement_id: [bid, d_hat, d_h_vot_max, vot_av], movement_id: [bid, d_hat, d_h_vot_max, vot_av], ... }
        self.timer = -1
        self.bidding_open = False
        self.timeout = G_MAX
        self.winning_combo = {}
        self.previous_winning_combo = set()
        self.clearance = False
        self.partial_clearance = False
        self.drivers = []
        self.not_full_capacity = False
        self.sub_bid = False
        self.next_traffic_signal = 'rrrrrrrr'
        self.mc_log = {}
        self.bidding_open_index = -1

        self.init_mc_log()


    ####################################################################################################################################
    # Action functions
    ####################################################################################################################################    

    def init_mc_log(self):
        for combo in VALID_COMBO_LIST:
            self.mc_log[str(sorted(combo))] = 'na'
    
    def receive_bid_offer(self, bid_offer, movement_id):
        self.bids[movement_id] = bid_offer 

    def receive_bid(self, bid):
        self.bank = self.bank + bid

    def decide_winner_and_reset_timer(self, movement_managers):
        if self.timer < self.timeout:
            self.calc_sub_bid()
        else:
            best_bids = []
            d_hat_combo_e, d_h_vot_max_combo_e, xe = self.get_enfranchised_variables(movement_managers)
            enfranchised_bid = self.get_enfranchised_bid()
            x_list, x_flag = self.get_x_variables(xe)
            c_soc = self.get_c_soc(movement_managers)

            for combo in VALID_COMBO_LIST:
                x_flag, x_list, best_bids = self.get_disenfranchised_variables_and_bid(combo, movement_managers, x_list, x_flag, enfranchised_bid, best_bids, d_hat_combo_e, c_soc)

            self.decide_winner_and_check_capacity(x_flag, x_list, best_bids)
            self.reset_timer()

        return self.winning_combo

    def update(self, traci):
        self.timer = self.timer + 1        
        print('Municipality timer: ' + str(self.timer))
        print('Municipality timeout: ' + str(self.timeout))
        if self.timer > self.timeout:
            self.bidding_open = True
            self.bidding_open_index = traci.simulation.getCurrentTime()*1.0/1000 -1
            self.sub_bid = False
            print('\nBIDDING ROUND OPEN')
        if self.not_full_capacity and self.timeout - self.timer > SATURATION_HEADWAY:
            self.bidding_open = True
            self.bidding_open_index = traci.simulation.getCurrentTime()*1.0/1000 -1
            self.sub_bid = True
            print('\nSUB-BIDDING ROUND OPEN')

    def close_bidding(self):
        self.bidding_open = False
        self.bids = {}
        if not self.sub_bid:
            self.timer = 0
            print('\nBIDDING ROUND CLOSED\n')
        else:
            print('\nSUB-BIDDING ROUND CLOSED\n')

    def compute_new_traffic_signal(self, winning_movement_ids, traci):
        code = list('rrrrrrrr')
        list_winning_movement_ids = list(winning_movement_ids)
        for index, item in enumerate(list_winning_movement_ids):
            code[MOVEMENT_TO_INDEX[item]] = ON
        self.next_traffic_signal = ''.join(code)

    def change_traffic_signal(self, winning_movement_ids, traci):
        print('Changing traffic signal to ' + str(self.next_traffic_signal))
        traci.trafficlight.setRedYellowGreenState(TRAFFIC_LIGHT_ID, self.next_traffic_signal)

    def enforce_clearance(self, traci):
        current_traffic_signal = traci.trafficlight.getRedYellowGreenState(TRAFFIC_LIGHT_ID)
        
        list_next_traffic_signal = list(ord(i) for i in list(self.next_traffic_signal))
        list_current_traffic_signal = list(ord(i) for i in list(current_traffic_signal))

        difference_map = [x[0]-x[1] for x in zip(list_next_traffic_signal, list_current_traffic_signal)]

        for index, value in enumerate(difference_map):
            if value > 0:
                list_current_traffic_signal[index] = ord(AMBER)        

        clearance_traffic_signal = ''.join([chr(i) for i in list_current_traffic_signal])

        traci.trafficlight.setRedYellowGreenState(TRAFFIC_LIGHT_ID, clearance_traffic_signal)        # Set lights to amber

    
    ####################################################################################################################################
    # Ancillary functions
    ####################################################################################################################################            

    def calc_sub_bid(self):
        best_movement_id = max(self.bids, key=lambda v: self.bids[v][0])
        print('\nBids:')
        
        for movement_id, bid in self.bids.iteritems():
            print('\t' + str(bid[0]) + '\t' + str(movement_id))

        best_bid = self.bids[best_movement_id][0]
        if best_bid > 0.0:
            self.winning_combo = self.previous_winning_combo | {best_movement_id}
            print('\nAdding movement to current winner. New combo:' + str(self.winning_combo) + '\nWinning bid: ' + str(best_bid))
            self.previous_winning_combo = self.winning_combo
        else:
            self.winning_combo = self.previous_winning_combo
            print('\nNo new movement to join with current winner.')
        self.clearance = False
        self.partial_clearance = False

    def get_enfranchised_variables(self, movement_managers):
        d_hat_combo_e = 0
        normalizer = 0

        d_h_vot_max_combo_e = 0
        xe = 0

        for bid in self.previous_winning_combo:
            d_hat = self.bids[bid][1]           # Get average estiamted wait time from manager who made bid
            d_h_vot_max = self.bids[bid][2]     # Get mac high VOT estiamted wait time from manager who made bid
            
            lenght_of_queue = len(movement_managers[bid].queue)

            d_hat_combo_e = d_hat_combo_e + d_hat*lenght_of_queue       # Premultiply manager's average estiamted wait time to perform unbiased averaging
            normalizer = normalizer + lenght_of_queue                   # Increment normalizer by the premultiplier for later normalization

            if d_h_vot_max > d_h_vot_max_combo_e:
                d_h_vot_max_combo_e = d_h_vot_max                       

        if normalizer > 0:
            d_hat_combo_e = d_hat_combo_e*1.0 / normalizer              # Perform normalization to get final enfranchised combo waiting time average

        if d_hat_combo_e > 0:
            xe = ((d_h_vot_max_combo_e - d_hat_combo_e)*1.0 / d_hat_combo_e)*100     

        # print('d_hat_combo_e, d_h_vot_max_combo_e, xe', d_hat_combo_e, d_h_vot_max_combo_e, xe)   

        return d_hat_combo_e, d_h_vot_max_combo_e, xe

    def get_disenfranchised_variables_and_bid(self, combo, movement_managers, x_list, x_flag, enfranchised_bid, best_bids, d_hat_combo_e, c_soc):
        bid = 0
        d_hat_combo = 0
        normalizer = 0

        d_h_vot_max_combo = 0
        x = 0

        for bidder in combo:
            d_hat = self.bids[bidder][1] 
            d_h_vot_max = self.bids[bidder][2] 
            lenght_of_queue = len(movement_managers[bidder].queue)

            d_hat_combo = d_hat_combo + d_hat*lenght_of_queue
            normalizer = normalizer + lenght_of_queue

            if d_h_vot_max > d_h_vot_max_combo:
                d_h_vot_max_combo = d_h_vot_max

        if normalizer > 0:
            d_hat_combo = d_hat_combo*1.0 / normalizer

        if d_hat_combo > 0:
            x = ((d_h_vot_max_combo - d_hat_combo)*1.0 / d_hat_combo)*100

        if x_flag is not True:            
            lenght_of_queue = 0
            for item in combo:
                lenght_of_queue = lenght_of_queue + len(movement_managers[item].queue)
                bid = bid + self.bids[item][0]
            
            lenght_of_queue_prev = 0
            for item in self.previous_winning_combo:
                lenght_of_queue_prev = lenght_of_queue_prev + len(movement_managers[item].queue)
            
            mc = ((d_hat_combo_e + G_MAX + 2*CLEARANCE)*lenght_of_queue_prev*10 - (d_hat_combo + G_EXT + CLEARANCE)*lenght_of_queue)*c_soc*0.0025
            
            # mc = ((d_hat_combo_e + G_MAX + 2*CLEARANCE)*10 - (d_hat_combo + G_EXT + CLEARANCE))*c_soc*0.025
            self.mc_log[str(sorted(combo))] = mc

            print('\ncombo:\t' + str(combo) + ' \tbid:\t' + str(bid)[0:3] + '\td_hat_combo:\t' + str(d_hat_combo) + ' \tmc: \t' + str(mc))

            if (bid > enfranchised_bid + mc) and combo != self.previous_winning_combo and bid >= INITIAL_FEE:
                best_bids.append([bid, combo])

        return x_flag, x_list, best_bids

    def decide_winner_and_check_capacity(self, x_flag, x_list, best_bids):
        print('\nBest bids:')
        for bid in best_bids:
            print('\t' + str(bid[0]) + '\t' + str(bid[1]))

        if x_flag is True and x_flag is False:
            self.winning_combo = max(x_list, key=lambda i: i[0])[1]
        else:
            if len(best_bids) == 0:
                self.winning_combo = self.previous_winning_combo  
            else:
                self.winning_combo = max(best_bids, key=lambda i: i[0])[1]
            
        self.not_full_capacity = False
        for item in self.winning_combo:
            if self.bids[item][0] == 0.0:
                self.winning_combo = self.winning_combo - {item}
                self.not_full_capacity = True

    def reset_timer(self):
        print('\nWinning combo: \t \t' + str(self.winning_combo))   
        print('\nPr. Winning combo: \t' + str(self.previous_winning_combo))   

        if len(self.winning_combo) == 0:
            self.timeout = 0
        elif len(self.previous_winning_combo) == 0:
            self.timeout = G_MAX
        elif self.winning_combo in self.previous_winning_combo or self.previous_winning_combo == self.winning_combo:
            self.timeout = G_EXT
        else:
            self.timeout = G_MAX

        
        if self.winning_combo == self.previous_winning_combo:
            self.clearance = False
            self.partial_clearance = False            
        elif len(self.winning_combo) == 0:
            self.clearance = True
            self.partial_clearance = False
        elif len(self.previous_winning_combo) == 0:
            self.clearance = False
            self.partial_clearance = False
        elif self.previous_winning_combo in self.winning_combo:
            self.clearance = False
            self.partial_clearance = False
        elif ( 0   <   len(self.previous_winning_combo & self.winning_combo)   <   len(self.previous_winning_combo)) or (((self.previous_winning_combo & self.winning_combo) in VALID_COMBO_LIST) and (len(self.winning_combo)*len(self.previous_winning_combo) > 0)):
            self.clearance = True
            self.partial_clearance = True
        else:
            self.clearance = True
            self.partial_clearance = False


        print('\nClearance: \t' + str(self.clearance))
        print('\nPartial Clearance: \t' + str(self.partial_clearance))
        print('\nTimeout: \t' + str(self.timeout))

        self.previous_winning_combo = self.winning_combo    

    def get_x_variables(self, xe):
        x_list = [[xe, self.previous_winning_combo]]
        x_flag = (xe > 0)        
        return x_list, x_flag    

    def get_enfranchised_bid(self):
        bid = 0
        for item in self.previous_winning_combo:
            bid = bid + self.bids[item][0]
        return bid

    def get_c_soc(self, movement_managers):
        c_soc = 0
        total_length = 0
        for movement_id, bid in self.bids.iteritems():
            lenght_of_queue = len(movement_managers[movement_id].queue)
            total_length = total_length + lenght_of_queue
            c_soc = c_soc + bid[3]*lenght_of_queue*0.2
        if total_length > 0:
            return c_soc*1.0 /total_length
        else:    
            return 0
