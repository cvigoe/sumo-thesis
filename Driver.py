from __future__ import division

import pymc3 as pm
import numpy as np
from simulation_constants import *

import seaborn as sns
import matplotlib.pyplot as plt
import math


class Driver(object):

    def __init__(self, driver_id, vot, initial_queue_pos, movement_manager, traci, saturation_headway=SATURATION_HEADWAY, bank=INITIAL_BANK, initial_fee=INITIAL_FEE, visible=False):
        self.driver_id = driver_id
        self.vot = vot                  # Value of Time - assume fixed for a given driver
        self.queue_pos = initial_queue_pos
        self.saturation_headway = saturation_headway
        self.bank = bank
        self.movement_manager = movement_manager
        self.alpha_prior = 1
        self.alpha_posterior = 1
        self.beta_prior = 1
        self.beta_posterior = 1
        self.k = 0
        self.n = 0
        self.timer = 0
        self.visible = visible
        self.initial_fee_unpaid = True

        self.state = 0
        self.pref = 0.5
        self.time_left = REPLENSIH

        self.set_up_initial_road_and_lane(traci)
        self.set_up_rho()        
    

    ####################################################################################################################################
    # Initialisation functions
    ####################################################################################################################################

    def set_up_initial_road_and_lane(self, traci):
        self.initial_road = traci.vehicle.getRoadID(self.driver_id)
        self.initial_lane = traci.vehicle.getLaneID(self.driver_id)

    def set_up_rho(self):
        self.rho = (self.alpha_prior + self.beta_prior) / self.alpha_prior      # Analytical form for mean(X) wehre X ~ Beta(alpha, beta)
    
    def set_up_estimated_delay(self, traci):
        self.initial_estimated_delay = self.get_ideal_run(traci) + (self.queue_pos*(self.saturation_headway)*self.rho)*2    # For 8 flows, unbiased odds of winning is 1/4
        self.estimated_delay = self.initial_estimated_delay

    def pay_initial_fee(self, initial_fee, traci):
        if self.visible and self.initial_fee_unpaid:
            self.bank = self.bank - initial_fee     # Assume a driver can always pay initial fee for now
            self.movement_manager.receive_initial_fee(initial_fee)
            self.initial_fee_unpaid = False
            self.queue_pos = self.get_initial_queue_pos()
            self.set_up_estimated_delay(traci)

    def get_initial_queue_pos(self):
        self.driver_id, self.movement_manager

    def get_initial_queue_pos(self):
        queue = self.movement_manager.queue 
        initial_queue_pos = len(queue) - list(queue).index(self.driver_id)
        print('\nNEW VISIBLE DRIVER: ' + str(self.driver_id))
        print('\tQueue: ' + str(queue))
        print('\tInitial pos: ' + str(initial_queue_pos) + '\n')
        return initial_queue_pos
    
    ####################################################################################################################################
    # Update functions
    ####################################################################################################################################
    
    def update_result(self):
        result = self.movement_manager.bidding_history[-1][1]
        
        if result == WIN:
            print('\tOutcome was WIN')
            print('\tOld k: ' + str(self.k) + '\t New k: ' + str(self.k + 1))
            print('\tOld n: ' + str(self.n) + '\t New n: ' + str(self.n + 1))
            self.k = self.k + 1
            self.n = self.n + 1
        elif result == LOSS:
            print('\tOutcome was LOSS')
            print('\tOld k: ' + str(self.k) + '\t New k: ' + str(self.k))
            print('\tOld n: ' + str(self.n) + '\t New n: ' + str(self.n + 1))
            self.n = self.n + 1
        elif result == NOT_ALLOWED:
            print('\tWas not allowed participate.')

    def update_theta_belief(self):
        self.alpha_posterior = self.alpha_prior + self.k
        print('\tAlpha prior: ' + str(self.alpha_prior) + '\t Alpha posterior: ' + str(self.alpha_posterior))
        self.beta_posterior = self.beta_prior + self.n - self.k
        print('\tBeta prior: ' + str(self.beta_prior) + '\t Beta posterior: ' + str(self.beta_posterior))
        new_rho = (self.alpha_posterior + self.beta_posterior) / self.alpha_posterior
        print('\tOld rho: ' + str(self.rho) + '\t New rho: ' + str(new_rho))
        self.rho = new_rho

    def update_queue_pos(self):
        queue = self.movement_manager.queue 
        new_queue_pos = len(queue) - list(queue).index(self.driver_id)
        print('\tOld queue pos: ' + str(self.queue_pos) + '\t Updated queue pos: ' + str(new_queue_pos))
        self.queue_pos = new_queue_pos

    def update_timer(self):
        new_time = self.timer + self.movement_manager.municipality.timeout + self.movement_manager.municipality.clearance*CLEARANCE
        print('\tOld timer: ' + str(self.timer) + '\t Updated timer: ' + str(new_time))
        self.time_left = self.time_left - 1
        if self.time_left == 0:
            self.time_left = REPLENSIH
            self.bank = INITIAL_CREDIT
        self.timer = new_time

    def update_estimated_delay(self, traci):
        # new_delay = self.timer + self.get_ideal_run(traci) + (self.queue_pos*(self.saturation_headway + TRAVEL_LOSS)*self.rho)                          
        # new_delay = traci.vehicle.getAccumulatedWaitingTime(self.driver_id) + (self.queue_pos*(self.saturation_headway + TRAVEL_LOSS)*self.rho)                          
        new_delay = traci.vehicle.getAccumulatedWaitingTime(self.driver_id) 
        print('\tOld est. delay: ' + str(self.estimated_delay) + '\t Updated est. delay: ' + str(new_delay) + '\t Initial est. delay: ' + str(self.initial_estimated_delay))
        self.estimated_delay = new_delay        

    def get_ideal_run(self, traci):
        dist = 0
        for i in range(len(traci.vehicle.getPosition(self.driver_id))):
            dist = dist + traci.vehicle.getPosition(self.driver_id)[i]*traci.vehicle.getPosition(self.driver_id)[i] - traci.junction.getPosition(JUNCTION_ID)[i]*traci.junction.getPosition(JUNCTION_ID)[i]
        return math.sqrt(abs(dist))*1.0 / SPEED_LIM

    def update_vot(self):
        print('\tOld VOT: ' + str(self.vot) + '\t Updated VOT: ' + str(self.vot))

    def take_posterior_as_prior(self):
        self.alpha_prior = self.alpha_posterior
        self.beta_prior = self.beta_posterior


    ####################################################################################################################################
    # Action functions
    ####################################################################################################################################    

    def decide_on_voluntary_contribution(self):
        # bayes_factor = self.compute_bayes_factor()
        # voluntary_contribution = self.vot*(10/(1 + 10*np.exp(self.initial_estimated_delay - self.estimated_delay)))/bayes_factor
        with pm.Model() as basic_model:
             p = pm.Beta('p', alpha=self.alpha_posterior, beta=self.beta_posterior)
             q = pm.Deterministic('q', 1 / p)
             out = pm.Binomial('out', n=1, p=p, observed=1)
             trace = pm.sample(500)

        rounds_till_win = trace[400]['q'] # sample drawn from the distribution of time-till-win
        epsilon = (self.timer + (self.queue_pos*SATURATION_HEADWAY*rounds_till_win) - self.initial_estimated_delay)
        if epsilon > 0:
            state = 0
        else:   
            state = sigmoid(epsilon)

        voluntary_contribution = self.vot*state*(self.bank/self.time_left)
        print('\tVoluntary contribution: ' + str(voluntary_contribution*ACTIVE))
        return voluntary_contribution*ACTIVE

    def compute_bayes_factor(self):
        return (self.alpha_posterior / (self.alpha_posterior + self.beta_posterior)) / (self.alpha_prior / (self.alpha_prior + self.beta_prior))

    def make_voluntary_contribution(self, voluntary_contribution):
        self.bank = self.bank - voluntary_contribution
        self.movement_manager.receive_voluntary_contribution(voluntary_contribution, self.timer)      




def sigmoid(x):
  return 1 / (1 + math.exp(-x))  
