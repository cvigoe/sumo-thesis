from __future__ import division

from collections import deque
import pymc3 as pm
import numpy as np
from simulation_constants import *


class Manager(object):

    def __init__(self, movement_id, municipality, lane_id, initial_queue_length=0, bank=1000.0, alpha=0.05, beta=0.0, gamma=0.02):
        self.movement_id = movement_id
        self.municipality = municipality
        self.lane_id = lane_id
        self.queue_length = initial_queue_length
        self.bank = bank  
        self.bidding_history = [[10.0,1]]                           # bidding_history = [ [bid, outcome], [bid,outcome], [bid,outcome], ... ]
        self.n = 0
        self.k = 0
        self.queue = deque()                                        # queue = [ driver_id, driver_id, driver_id, driver_id ]  FIFO list
        self.N = 10                                                 # N for bid calculation (last N bids used)
        self.drivers = {}
        self.alpha = alpha
        self.beta = beta
        self.gamma = gamma                                          # bid = INITIAL_FEE + alpha*queue_length + beta*bank + gamma*bids_since_win
        self.d_hat = -1
        self.d_PERC = -1
        self.weighted_contributions_from_this_round = 0
    ####################################################################################################################################
    # Update functions
    ####################################################################################################################################
    
    def update_outcome_history(self, winning_movement_ids):
        bid = self.bidding_history[-1][0]
        allowed = True
        
        if bid == 0.0:
            allowed = False

        if self.movement_id in winning_movement_ids and allowed:
            self.bidding_history[-1][1] = WIN
            self.k = self.k + 1 
            self.n = self.n + 1
        elif self.movement_id not in winning_movement_ids and allowed:
            self.bidding_history[-1][1] = LOSS
            self.n = self.n + 1
        else:
            self.bidding_history[-1][1] = NOT_ALLOWED            


    ####################################################################################################################################
    # Action functions
    ####################################################################################################################################    

    def receive_initial_fee(self, initial_fee):
        self.bank = self.bank + initial_fee

    def receive_voluntary_contribution(self, voluntary_contribution, wait_time):
        self.bank = self.bank + voluntary_contribution
        self.weighted_contributions_from_this_round = self.weighted_contributions_from_this_round + voluntary_contribution*wait_time

    def make_bid(self):
        d_hat, d_h_vot_max, vot_av = self.get_queue_params()

        if self.check_if_allowed_to_participate():
            last_non_zero_N_bids = self.get_last_non_zero_N_bids()
            bid = self.calculate_bid(last_non_zero_N_bids)

            self.bidding_history.append([bid, IN_PROGRESS])
            self.municipality.receive_bid_offer([bid, d_hat, d_h_vot_max, vot_av], self.movement_id)
        else:
            bid = 0.0                            

            self.bidding_history.append([bid, IN_PROGRESS])
            self.municipality.receive_bid_offer([bid, d_hat, d_h_vot_max, vot_av], self.movement_id)

            print('\tNot allowed participate.')
            print('\tQueue: ' + str(self.queue))

        self.weighted_contributions_from_this_round = 0 # Reset contributions

    def get_queue_params(self):
        d_hat = 0
        vot_av = 0
        d_h_vot_max = 0
        self.queue_length = len(self.queue)
        stats = []

        for driver_id in self.queue:
            if self.drivers[driver_id].vot > 1:
                if self.drivers[driver_id].estimated_delay > d_h_vot_max:
                    d_h_vot_max = self.drivers[driver_id].estimated_delay

            d_hat = d_hat + self.drivers[driver_id].estimated_delay
            vot_av = vot_av + self.drivers[driver_id].vot
            stats.append(self.drivers[driver_id].estimated_delay)
        
        if self.queue_length > 0:
            d_hat = d_hat*1.0 / self.queue_length
            vot_av = vot_av*1.0 / self.queue_length  

        self.d_hat = d_hat
        if len(stats) > 1:
            self.d_PERC = np.percentile(stats, DELAY_PERCENTILE)
        else:
            self.d_PERC = -1

        return d_hat, d_h_vot_max, vot_av

    def calculate_bid(self, last_non_zero_N_bids):
        average_non_zero_bid_over_N = np.mean([bid[0] for bid in last_non_zero_N_bids])
        prob_loss_over_N = 1.0 - sum([bid[1] for bid in last_non_zero_N_bids])*1.0 / len(last_non_zero_N_bids)
        
        bids_since_win = 0
        for bid in reversed(last_non_zero_N_bids):
            if bid[1] == LOSS:
                bids_since_win = bids_since_win + 1
            else:
                break

        percentage_high_VOT = self.get_perc_high_vot()
        
        # bid = min(average_non_zero_bid_over_N*(0.5 + prob_loss_over_N)*(1+percentage_high_VOT), self.bank)                # Bidding strategy from Acomm. High VOT paper

        bid = min(INITIAL_FEE + self.alpha*self.queue_length + self.beta*self.bank + self.gamma*bids_since_win, self.bank)  # Bidding strategy from All Passive Players paper

        EXPLOITATION_ACCUMULATION = 1
        bid = self.weighted_contributions_from_this_round*EXPLOITATION_ACCUMULATION


        # print('\taverage_non_zero_bid_over_N: ' + str(average_non_zero_bid_over_N) + '\tprob_loss_over_N: ' + str(prob_loss_over_N) + '\tpercentage_high_VOT: ' + str(percentage_high_VOT))
        print('\tlast non-zero N bids: ' + str(last_non_zero_N_bids))
        print('\tqueue_length: ' + str(self.queue_length) + '\tbank: ' + str(self.bank) + '\tbids_since_win: ' + str(bids_since_win)) 
        print('--->\tMaking bid: ' + str(bid))
        return bid
    
    def get_last_non_zero_N_bids(self):
        last_non_zero_N_bids = []
        for bid in self.bidding_history:
            if bid[0] > 0.0:
                last_non_zero_N_bids.append(bid)
            if len(last_non_zero_N_bids) == self.N:
                break      
        return last_non_zero_N_bids
        
    def make_payment(self):
        bid = self.bidding_history[-1][0]
        self.bank = self.bank - bid
        self.municipality.receive_bid(bid)

    def check_if_allowed_to_participate(self):
        if len(self.queue) == 0:
            return False

        if self.municipality.timer >= self.municipality.timeout:
            return True
        else:
            if self.municipality.not_full_capacity:
                test_combo = self.municipality.previous_winning_combo | {self.movement_id}
                return (test_combo in VALID_COMBO_LIST) or (len(self.municipality.previous_winning_combo) == 0)
        return False

    def get_perc_high_vot(self):
        total = len(self.queue)
        high = 0
        for driver_id, driver in self.drivers.iteritems():
            if driver.driver_id in self.queue and driver.vot > 1:
                high = high + 1
        if total > 0:
            return high * 1.0 / total
        else:
            return 0.0
