import pymc3 as pm
from scipy import optimize
import matplotlib.pyplot as plt
import seaborn as sns; sns.set(color_codes=True)

with pm.Model() as basic_model:
     p = pm.Uniform('p', lower=0, upper=1)
     q = pm.Deterministic('q', 1 / p)
     out = pm.Binomial('out', n=1, p=p, observed=1)
     trace = pm.sample(500)

rounds_till_win = trace[400]['q'] # sample drawn from the distribution of time-till-win
epsilon = (self.timer + (self.queue_pos*SATURATION_HEADWAY*rounds_till_win) - self.initial_estimated_delay)
if epsilon > 0:
	state = 0
else:	
	state = sigmoid(epsilon)

contribution = self.VOT*state*(self.bank/self.rounds_left)

pm.traceplot(trace)	
plt.show()