import json
import matplotlib.pyplot as plt
import seaborn as sns; sns.set(color_codes=True)
import numpy as np

# acc_loaded_data = json.load(open('actuated/wait_time_log_actuated.json'))
acc_loaded_data = json.load(open('json_logs.json'))
loaded_data = json.load(open('json_logs.json'))
keys = loaded_data.keys()

cmap = plt.get_cmap('plasma')
N = len(keys)

for index, key in enumerate(keys):
	# Append 100 to data to end all distributions at same point
	acc_data = acc_loaded_data[key]
	print(acc_data)
	# Choose how many bins you want here
	num_bins = 100
	# Use the histogram function to bin the data
	acc_counts, acc_bin_edges = np.histogram(acc_data, bins=num_bins, density=True)
	# Now find the cdf, normalizing for non-unity bin width
	acc_cdf = np.cumsum(acc_counts)*(acc_bin_edges[2] - acc_bin_edges[1])
	# And finally plot the cdf, appending 0 to complete distribution plot
	color = cmap(float(index)/N)
	plt.plot(np.append([0], acc_bin_edges[1:]), np.append([0], acc_cdf), '--',label='Actuated', color=color, lw=2)
	# Append 100 to data to end all distributions at same point
	data = loaded_data[key]
	print(data)
	# Choose how many bins you want here
	num_bins = 100
	# Use the histogram function to bin the data
	counts, bin_edges = np.histogram(data, bins=num_bins, density=True)
	# Now find the cdf, normalizing for non-unity bin width
	cdf = np.cumsum(counts)*(bin_edges[2] - bin_edges[1])
	# And finally plot the cdf, appending 0 to complete distribution plot
	color = cmap(float(index)/N)
	plt.plot(np.append([0], bin_edges[1:]), np.append([0], cdf), label='Bid-based', color=color, lw=2)
	plt.title('CDFs of Accumulaterd Wait Time\n' + key)
	plt.ylabel('CDF')
	plt.xlabel('Accumulated Wait Time (s)')
	plt.legend(loc='lower right')
	plt.ylim([0,1])
	plt.xlim([0,300])
	plt.savefig('cdfs/' + key + '_CDF_comparison_2.png')
	plt.clf()
	# plt.show()




# import json
# import matplotlib.pyplot as plt
# import seaborn as sns; sns.set(color_codes=True)
# import numpy as np

# loaded_data = json.load(open('wait_time_log.json'))
# keys = loaded_data.keys()

# cmap = plt.get_cmap('gist_ncar')
# N = len(keys)

# for index, key in enumerate(keys):
# 	# Append 100 to data to end all distributions at same point
# 	data = loaded_data[key] + [300]
# 	print(data)
# 	# Choose how many bins you want here
# 	num_bins = 100
# 	# Use the histogram function to bin the data
# 	counts, bin_edges = np.histogram(data, bins=num_bins, density=True)
# 	# Now find the cdf, normalizing for non-unity bin width
# 	cdf = np.cumsum(counts)*(bin_edges[2] - bin_edges[1])
# 	# And finally plot the cdf, appending 0 to complete distribution plot
# 	color = cmap(float(index)/N)
# 	plt.plot(np.append([0],bin_edges[1:]), np.append([0],cdf), label=key, color=color)

# plt.title('CDFs of Accumulaterd Wait Time for Each Movement\nBID-BASED')
# plt.ylabel('CDF')
# plt.xlabel('Accumulated Wait Time (s)')
# plt.legend(loc='lower right')
# plt.ylim([0,1])
# plt.show()