<?xml version="1.0" encoding="UTF-8"?>

<!-- generated on Sat Mar 24 15:22:09 2018 by SUMO Version 0.32.0
<?xml version="1.0" encoding="UTF-8"?>

<configuration xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://sumo.dlr.de/xsd/sumoConfiguration.xsd">

    <input>
        <net-file value="isolated.net.xml"/>
        <route-files value="isolated.rou.xml"/>
        <additional-files value="isolated_additionals.xml"/>
    </input>

    <output>
        <tripinfo-output value="tripinfo.xml"/>
    </output>

    <time>
        <begin value="0"/>
        <end value="10000"/>
        <step-length value="1"/>
    </time>

    <processing>
        <time-to-teleport value="-1"/>
        <waiting-time-memory value="500"/>
    </processing>

    <traci_server>
        <remote-port value="52173"/>
    </traci_server>

    <gui_only>
        <gui-settings-file value="isolated.settings.xml"/>
        <tracker-interval value="1"/>
    </gui_only>

</configuration>
