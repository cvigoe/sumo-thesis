from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import matplotlib as mpl
mpl.use('TkAgg')    # Required for plotting whilst using XQuartz 
import matplotlib.pyplot as plt

import os
import sys
import optparse
import subprocess
import random
from sumolib import checkBinary 
import traci
import numpy as np
import pymc3 as pm
import seaborn as sns

from Driver import *
from Manager import *
from Municipality import *

import simulation_constants

import csv

import json


####################################################################################################################################
# Simulation control logic
####################################################################################################################################

def run():
    municipality = create_municipality(traci)
    movement_managers = create_movement_managers(municipality)
    drivers = []
    logs = []
    json_logs = {}
    hl, h2, figure, ax, analysis  = set_up_figure_and_analysis()
    json_logs = init_logs(json_logs)

    while traci.simulation.getMinExpectedNumber() > 0:
        simulation_step(traci)
        IDList = traci.simulation.getDepartedIDList()               # Get the list of loaded vehicles since the last simulation step
        
        drivers = remove_serviced_drivers(drivers, traci, municipality, json_logs)
        drivers = check_for_new_drivers(IDList, movement_managers, drivers, traci)
        municipality.update(traci)                                       # Update the municipality to determine if bidding is open

        if municipality.bidding_open:
            update_driver_parameters_and_contributions(drivers, traci)
            update_managers_and_make_bids(movement_managers, drivers)
            decide_winner_reset_timer_and_make_payment(municipality, movement_managers, traci)
            update_driver_beliefs(drivers)            
            municipality.close_bidding()

            update_analysis(drivers, analysis, traci)

            municipality.compute_new_traffic_signal(municipality.winning_combo, traci)
            if municipality.clearance:
                municipality.enforce_clearance(traci)
                drivers = continue_simulation_for_clearance(traci, drivers, movement_managers, municipality, hl, h2, figure, ax, analysis, logs, json_logs)

            municipality.change_traffic_signal(municipality.winning_combo, traci)
        
        update_analysis_plot(hl, h2, figure, ax, analysis, logs, traci, movement_managers, municipality)

    with open('json_logs.json', 'w') as f:
        json.dump(json_logs, f)

    traci.close()
    sys.stdout.flush()


####################################################################################################################################
# Environment setup functions
####################################################################################################################################

def create_municipality(traci):
    municipality = Municipality()
    traci.trafficlight.setRedYellowGreenState(TRAFFIC_LIGHT_ID, 'rrrrrrrr')
    return municipality

def init_logs(json_logs):
    log = ['Municipality Timer (s)', 'Simulation Time (s)']

    for movement in sorted(MOVEMENTS):
        log.append(movement + ' queue length')
        json_logs[movement] =  []

    for movement in sorted(MOVEMENTS):
        log.append(movement + " bid")

    for combo in VALID_COMBO_LIST:
        log.append(str(sorted(combo)) + " MC")
    
    for movement in sorted(MOVEMENTS):
        log.append(movement + " av. delay")
        log.append(movement + ' ' + str(DELAY_PERCENTILE) + "-tile delay")

    log.append('Light Sequence')

    with open('log_csv.csv', 'a') as myfile:
        wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
        wr.writerow(log)    

    return json_logs

def create_movement_managers(municipality):
    movement_managers = {}
    for movement in MOVEMENTS:
        lane_id = LANE_ID_LOOKUP[movement]
        manager = Manager(movement, municipality, lane_id)
        movement_managers[movement] = manager
    return movement_managers

def has_been_serviced(driver, traci): # *
    if not driver.visible:
        return False

    lane = traci.vehicle.getLaneID(driver.driver_id)
    
    if 'to' in lane or 'junction' in lane:
        print('Serviced driver: ' + driver.driver_id)
        return True
    elif 'to' not in lane and 'from' not in lane:    #Edge case when car ON junction
        print('Serviced driver: ' + driver.driver_id)
        return True        
    else:
        return False

def get_vot(driver_id): # *
    return 1
    vot = np.random.random()
    if vot > 0.9:
        traci.vehicle.setColor(driver_id, (255,0,0,255))
        vot = 20*vot
    else:
        traci.vehicle.setColor(driver_id, (255,255,0,255))
    return vot   # VOT random between 0 and 1    

def assign_movement_manager(driver_id, movement_managers):
    movement = driver_id.split('.')[0]
    manager = movement_managers[movement]
    return manager

def set_up_figure_and_analysis():
    analysis = [-1,-1]
    
    figure, ax = plt.subplots()
    hl, = ax.plot([-2,-1], analysis)
    h2, = ax.plot([-2,-1], analysis)
    plt.ylim([0,100])
    plt.ylabel('Average Service Time (s)')        
    plt.xlabel('Simulation Index')
    plt.title('Average Service Time vs. Simulation Index (all movements)\nBID-BASED (alpha-beta-gamma bidding model)')
    plt.show(block=False)      

    return hl, h2, figure, ax, analysis


####################################################################################################################################
# Ancillary functions
####################################################################################################################################

def simulation_step(traci):
    traci.simulationStep()    
    print('\n*************************************************')   
    print('Simulation Index: ' + str(traci.simulation.getCurrentTime()*1.0/1000 -1) + '\n')

def update_logs(logs, traci, movement_managers, municipality):
    log = []
    
    allowed = (municipality.bidding_open_index == traci.simulation.getCurrentTime()*1.0/1000 -1)

    if (allowed and 'y' in traci.trafficlight.getRedYellowGreenState(TRAFFIC_LIGHT_ID)) or (not allowed and 'y' in traci.trafficlight.getRedYellowGreenState(TRAFFIC_LIGHT_ID)):
        log.append(-1)
    else:
        log.append(municipality.timer)

    log.append(traci.simulation.getCurrentTime()*1.0/1000 -1)

    for movement in sorted(MOVEMENTS):
        log.append(len(movement_managers[movement].queue))

    for movement in sorted(MOVEMENTS):
        if allowed:
            log.append(movement_managers[movement].bidding_history[-1][0])
        else:
            log.append(0)        

    for combo in VALID_COMBO_LIST:
        if allowed:
            log.append(municipality.mc_log[str(sorted(combo))])
        else:
            log.append(0)

    for movement in sorted(MOVEMENTS):
        log.append(movement_managers[movement].d_hat)
        log.append(movement_managers[movement].d_PERC)

    log.append(traci.trafficlight.getRedYellowGreenState(TRAFFIC_LIGHT_ID))
    # logs.append(log)
    with open('log_csv.csv', 'a') as myfile:
        wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
        wr.writerow(log)    

def get_options():
    optParser = optparse.OptionParser()
    optParser.add_option("--nogui", action="store_true",
                         default=False, help="run the commandline version of sumo")
    options, args = optParser.parse_args()
    return options

def remove_serviced_drivers(drivers, traci, municipality, json_logs):
    updated_drivers = []
    newline_flag = False
    for index, driver in enumerate(drivers):               
        if not has_been_serviced(driver, traci):
            updated_drivers.append(driver)
        else:
            newline_flag = True
            del driver.movement_manager.drivers[driver.driver_id]
            json_logs[driver.movement_manager.movement_id].append(traci.vehicle.getAccumulatedWaitingTime(driver.driver_id))
            with open('json_logs.json', 'w') as f:
                json.dump(json_logs, f)
            
    if newline_flag:
        print(' ')

    municipality.drivers = updated_drivers
    return updated_drivers

def check_for_new_drivers(IDList, movement_managers, drivers, traci):
    if len(IDList) != 0:                                   # If we have new drivers, create a Driver class instance and add them to the driver store
        for driver_id in IDList:
            print('New driver: ' + str(driver_id))
            vot = get_vot(driver_id)
            movement_manager = assign_movement_manager(driver_id, movement_managers)
            driver = Driver(driver_id, vot, -1, movement_manager, traci)
            movement_manager.drivers[driver_id] = driver
            drivers.append(driver)
            traci.vehicle.setLaneChangeMode(driver_id, 256)
        print(' ')
    
    for movement, manager in movement_managers.iteritems():
        manager.queue = traci.lane.getLastStepVehicleIDs(manager.lane_id)    

    for driver in drivers:
        driver.visible = ('pre' not in traci.vehicle.getRoadID(driver.driver_id) and 'junction' not in traci.vehicle.getRoadID(driver.driver_id))
        driver.pay_initial_fee(INITIAL_FEE, traci)

    return drivers

def update_driver_parameters_and_contributions(drivers, traci):
    print('\n- - - - - - - - - - - - - - - - - - - - - - - - -') 
    for index, driver in enumerate(drivers):            # Update every visible driver's beliefs; decide on voluntary payments
        if driver.visible:
            print('\nUpdating driver: ' + str(driver.driver_id))
            driver.update_queue_pos()
            driver.update_timer()
            driver.update_estimated_delay(traci)
            driver.update_vot()

            voluntary_contribution = driver.decide_on_voluntary_contribution()
            if voluntary_contribution > 0:
                driver.make_voluntary_contribution(voluntary_contribution)     

def update_managers_and_make_bids(movement_managers, drivers):
    print('\n- - - - - - - - - - - - - - - - - - - - - - - - -') 
    for movement, manager in movement_managers.iteritems():
        print('\nUpdating manager: ' + str(manager.movement_id))
        manager.make_bid()                                  

def decide_winner_reset_timer_and_make_payment(municipality, movement_managers, traci):
    print('\n- - - - - - - - - - - - - - - - - - - - - - - - -') 
    winning_movement_ids = municipality.decide_winner_and_reset_timer(movement_managers)     
    
    for winning_movement_id in winning_movement_ids:
        movement_managers[winning_movement_id].make_payment()

    for movement, manager in movement_managers.iteritems():
        manager.update_outcome_history(winning_movement_ids)  

def update_driver_beliefs(drivers):
    print('\n- - - - - - - - - - - - - - - - - - - - - - - - -') 
    for index, driver in enumerate(drivers):
        if driver.visible:
            print('\nUpdating driver based on outcome: ' + str(driver.driver_id))
            driver.update_result()
            driver.update_theta_belief()
            driver.take_posterior_as_prior()      
    print('\n- - - - - - - - - - - - - - - - - - - - - - - - -') 

def continue_simulation_for_clearance(traci, drivers, movement_managers, municipality, hl, h2, figure, ax, analysis, logs, json_logs):
    update_analysis_plot(hl, h2, figure, ax, analysis, logs, traci, movement_managers, municipality)
    for i in range(int(CLEARANCE)):
        print('Clearance ' + str(i + 1))   
        simulation_step(traci)
        IDList = traci.simulation.getDepartedIDList()               # Get the list of loaded vehicles since the last simulation step
    
        drivers = remove_serviced_drivers(drivers, traci, municipality, json_logs)
        drivers = check_for_new_drivers(IDList, movement_managers, drivers, traci)            
        update_analysis(drivers, analysis, traci)   
        if i < int(CLEARANCE) - 1:
            update_analysis_plot(hl, h2, figure, ax, analysis, logs, traci, movement_managers, municipality)
    return drivers

def update_analysis(drivers, analysis, traci):
    total_acc_delay = 0
    N = 0
    for driver in drivers:
        # total_acc_delay = total_acc_delay + traci.vehicle.getAccumulatedWaitingTime(driver.driver_id)
        total_acc_delay = total_acc_delay + traci.vehicle.getAccumulatedWaitingTime(driver.driver_id)
        # total_acc_delay = total_acc_delay + driver.timer
        N = N + 1

    if N > 0:
        analysis.append(total_acc_delay*1.0 / N)

def update_analysis_plot(hl, h2, figure, ax, analysis, logs, traci, movement_managers, municipality):
    update_logs(logs, traci, movement_managers, municipality)
    # x_data = list(hl.get_xdata())
    # x_data.append(len(x_data) + 1)
    # hl.set_xdata(x_data)

    # y_data = list(hl.get_ydata())
    # y_data.append(analysis[-1])
    # hl.set_ydata(y_data)

    # x_data = list(h2.get_xdata())
    # x_data.append(len(x_data) + 1)
    # h2.set_xdata(x_data)

    # av_data = list(h2.get_ydata())
    # if len(y_data) > MAF:
    #     av = np.mean(y_data[-MAF:])
    #     av_data.append(av)
    #     ax.legend(['Instantaneous Av. Service Time (s)', 'Moving Av. (past 100 values) (s)'])
    # else:
    #     av_data.append(-1)
    #     ax.legend(['Instantaneous Av. Service Time (s)'])
    # h2.set_ydata(av_data)
    
    # ax.relim()
    # ax.autoscale_view()
    # figure.canvas.draw()
    # figure.canvas.flush_events()    


####################################################################################################################################
# Main entry point to script
####################################################################################################################################

if __name__ == "__main__":
    options = get_options()

    # this script has been called from the command line. It will start sumo as a
    # server, then connect and run
    if options.nogui:
        sumoBinary = checkBinary('sumo')
    else:
        sumoBinary = checkBinary('sumo')

    # this is the normal way of using traci. sumo is started as a
    # subprocess and then the python script connects and runs
    traci.start([sumoBinary, "-c", "isolated.sumocfg", "--tripinfo-output", "tripinfo.xml", "--time-to-teleport", '-1', '--step-length', TIMESTEP_STR, '--waiting-time-memory', '500'])

    run()
